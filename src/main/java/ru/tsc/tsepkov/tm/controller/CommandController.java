package ru.tsc.tsepkov.tm.controller;

import ru.tsc.tsepkov.tm.api.ICommandController;
import ru.tsc.tsepkov.tm.api.ICommandService;
import ru.tsc.tsepkov.tm.model.Command;
import ru.tsc.tsepkov.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    public final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);

        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;

        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    @Override
    public void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument not supported...");
        System.exit(1);
    }

    @Override
    public void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command not supported...");
        System.exit(1);
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Evgeny Tsepkov");
        System.out.println("e-mail: markmilson@yandex.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
    }

}
